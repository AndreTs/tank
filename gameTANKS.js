/**
 * Created by Андрей on 25.07.2016.
 */

$(document).ready(function() {

    $(document).keydown(function (e) {
        //var tank = null;
        var tank = document.getElementById("tank");
        var x = tank.offsetLeft;
        var y = tank.offsetTop;
        var k = 10; //шаг
        switch (e.keyCode) {
            case 37:
                if ( x > 0 ){ tank.style.left = ( x -= k ) +"px"; tank.style.transform = 'rotate(180deg)'; }
                break;
            case 38:
                if ( y > 0 ){ tank.style.top = ( y -= k ) +"px"; tank.style.transform = 'rotate(270deg)'; }
                break;
            case 39:
                if ( x < 480 ){ tank.style.left = ( x += k ) +"px"; tank.style.transform = 'rotate(0deg)'; }
                break;
            case 40:
                if ( y < 480 ){ tank.style.top = ( y += k ) +"px"; tank.style.transform = 'rotate(90deg)'; }
                break;
        }
    });

    function botMove() {
        var k, dir;
        var bot = null;
        var bot = document.getElementById("bot");
        var x=0;
        var y=0;
        x = bot.offsetLeft;
        y = bot.offsetTop;

        setInterval(function() {
            dir = Math.floor(Math.random() * (4 - 1 + 1)) + 1;
            k = Math.floor(Math.random() * (30 - 10 + 1)) + 10;

            switch (dir) {
                case 1:
                    console.info("1")
                    if ( x > 0 ){ bot.style.left = ( x -= k ) +"px"; bot.style.transform = 'rotate(270deg)'; }
                    break;
                case 2:
                    console.info("2")
                    if ( y > 0 ){ bot.style.top = ( y -= k ) +"px"; bot.style.transform = 'rotate(0deg)'; }
                    break;
                case 3:
                    console.info("3")
                    if ( x < 480 ){ bot.style.left = ( x += k ) +"px"; bot.style.transform = 'rotate(90deg)'; }
                    break;
                case 4:
                    console.info("4")
                    if ( y < 480 ){ bot.style.top = ( y += k ) +"px"; bot.style.transform = 'rotate(180deg)'; }
                    break;
            }
        }, 1000)
    }

    botMove();

});
